#!/bin/bash

#GETFWINFO.BAT script for getting firmware details in one click by nutsey for GoPrawn.com
#@echo off
#setlocal
V3SIZE="12058624"
V3SSIZE="8388608"
FULLFEX=full_img.fex
SCRIPTBIN=script.bin
SCRIPTFEX=script.fex
INFOTXT=fwinfo.txt
#INFOTXTTMP=${INFOTXT}.tmp
INFOTXTTMP=${INFOTXT}

FULLFLEXFILESIZE="$(stat -c %s ${FULLFEX})"

CPYFOLDER=UNFEX/
UBOOT=${CPYFOLDER}0-uboot.img
BOOT=${CPYFOLDER}1-boot.img
SYSTEM=${CPYFOLDER}2-system.img
CONFIG=${CPYFOLDER}3-config.img
BOOTLOGO=${CPYFOLDER}4-blogo.img
SLOGO=${CPYFOLDER}5-slogo.img
ENV=${CPYFOLDER}6-env.img

ROOTEXTRACTFOLDER=squashfs-root/

function partCopy {
  INFILE=$1
  OUTFILE=$2
  dd if=${INFILE} of=${OUTFILE} bs=1 skip=204800 count=36272 status=none
}

function splitCopyV3 {
  INFILE=$1
  dd if=${INFILE} of=${UBOOT}    bs=64 skip=0        count=4096      status=none
  dd if=${INFILE} of=${BOOT}     bs=64 skip=4096     count=45056     status=none
  dd if=${INFILE} of=${SYSTEM}   bs=64 skip=49152    count=101376    status=none
  dd if=${INFILE} of=${CONFIG}   bs=64 skip=150528   count=32768     status=none
  dd if=${INFILE} of=${BOOTLOGO} bs=64 skip=183296   count=2048      status=none
  dd if=${INFILE} of=${SLOGO}    bs=64 skip=185344   count=2048      status=none
  dd if=${INFILE} of=${ENV}      bs=64 skip=187392   count=1024      status=none
}

function splitCopyV3S {
  INFILE=$1
  dd if=${INFILE} of=${UBOOT}    bs=1 skip=0        count=4096      status=none
  dd if=${INFILE} of=${BOOT}     bs=1 skip=262144   count=2818048   status=none
  dd if=${INFILE} of=${SYSTEM}   bs=1 skip=3080192  count=4587520   status=none
  dd if=${INFILE} of=${CONFIG}   bs=1 skip=7667712  count=524288    status=none
  dd if=${INFILE} of=${BOOTLOGO} bs=1 skip=8192000  count=65536     status=none
  dd if=${INFILE} of=${SLOGO}    bs=1 skip=8257536  count=65536     status=none
  dd if=${INFILE} of=${ENV}      bs=1 skip=8323072  count=65536     status=none
}

function convertBin {
  INFILE=$1
  OUTFILE=$2
  java -jar unscript.jar ${INFILE} 2>/dev/null | more > ${OUTFILE}
}

function squashfs_unmake {
  INFILE=$1
  OUTFILE=$2
  unsquashfs -f -n -d ${OUTFILE} ${INFILE} >/dev/null 2>&1
}

function extractFWInfo {
  ROOTFOLDER=$1
  FEXFILE=$2
  OUTFILE=$3

  TMP=$(mktemp)
  sed "s/\./_/g" ${ROOTFOLDER}/build.prop > ${TMP}
  source ${TMP} >/dev/null 2>&1
  rm ${TMP}
  
  TMP=$(mktemp)
  sed "s/(/_/g;s/)/_/g" ${ROOTFOLDER}/etc/MiniGUI.cfg > ${TMP}
  awk -i inplace 'BEGIN { cntr = 0 } /defaultmode=*/ { cntr++ ; print "_" cntr $0 } !/defaultmode=*/ { print $0 }' ${TMP}
  source <(grep = ${TMP}) >/dev/null 2>&1
  rm ${TMP}
  
  array=(${_1defaultmode//-/ })
  LCDRES=${array[0]}
  LCDBPP=${array[1]}
  
  TMP=$(mktemp)
  cp ${ROOTFOLDER}/res/cfg/${LCDRES}.cfg ${TMP}
  source <(grep = ${TMP}) >/dev/null 2>&1
  rm ${TMP}
  
  #TMP=$(mktemp)
  TMP=/tmp/ddd
  sed "s/\ =\ /=/g;/port:/d" ${FEXFILE} > ${TMP}
  source <(grep = ${TMP}) >/dev/null 2>&1
  #rm ${TMP}
  
  array=(${ro_build_version_incremental//_/ })
  FIRMWARDATE=${array[2]}


  echo Product:                               >> ${OUTFILE}
  echo "   "${ro_build_product}               >> ${OUTFILE}

  echo Manufacturer:                          >> ${OUTFILE}
  echo "   "${ro_build_user}                  >> ${OUTFILE}
  
  if [ -f ${ROOTFOLDER}/etc/info.xml ]; then
    echo "ERROR: /etc/info.xml isn't yet supported..."
    exit 1

    # echo Short id:  >> fwinfo.tmp
    # .\sfk extract squashfs-root\etc\info.xml -text "/<Short>*</[part2]/" -astext >> fwinfo.tmp
    # echo OEM id:  >> fwinfo.tmp
    # .\sfk extract squashfs-root\etc\info.xml -text "/<SubClient>*</[part2]/" -astext >> fwinfo.tmp
    # echo Camera type:  >> fwinfo.tmp
    # .\sfk extract squashfs-root\etc\info.xml -text "/<CamType>*</[part2]/" -astext >> fwinfo.tmp
    # echo FW orig. date:  >> fwinfo.tmp
    # .\sfk extract squashfs-root\build.prop -text "/ro.build.version.incremental=*.*.*.*/[part6]/" -astext >> fwinfo.tmp
    # echo FW mod. date:  >> fwinfo.tmp
    # .\sfk extract squashfs-root\etc\info.xml -text "/<FirmDate>*</[part2]/" -astext >> fwinfo.tmp
  else
    echo FW date:                              >> ${OUTFILE}
    echo "   "${FIRMWARDATE}                   >> ${OUTFILE}
  fi

  echo Camera name:                           >> ${OUTFILE}
  echo "   "${product_type}                   >> ${OUTFILE}
  
  echo Software version:                      >> ${OUTFILE}
  echo "   "${software_version}               >> ${OUTFILE}
  
  echo LCD model:                             >> ${OUTFILE}
  echo "   "${lcd_driver_name}                >> ${OUTFILE}
  
  echo LCD resolution:                        >> ${OUTFILE}
  echo "   "${LCDRES}                         >> ${OUTFILE}
  
  if [ -f ${ROOTFOLDER}/etc/info.xml ]; then
    echo "ERROR: /etc/info.xml isn't yet supported..."
    exit 1
  fi

  echo Sensor model:                          >> ${OUTFILE}
  echo "   "${vip_dev0_mname}                 >> ${OUTFILE}

}

rm -rf ${CPYFOLDER}
rm -rf ${ROOTEXTRACTFOLDER}

mkdir -p ${CPYFOLDER}
mkdir -p ${ROOTEXTRACTFOLDER}

echo Auto FWINFO script by nutsey v0.4.
echo -n
echo  [====--------------------] 10%%
partCopy ${FULLFEX} ${SCRIPTBIN}

echo  [========----------------] 20%%
convertBin ${SCRIPTBIN} ${SCRIPTFEX}

echo  [============------------] 40%%
echo SoC:  > ${INFOTXTTMP}
if [ "${FULLFLEXFILESIZE}" == "$V3SIZE" ]; then
  echo "   Allwinner V3" >> ${INFOTXTTMP}
  splitCopyV3 ${FULLFEX}
elif [ "${FULLFLEXFILESIZE}" == "$V3SSIZE" ]; then
  echo "   Allwinner V3S" >> ${INFOTXTTMP}
  splitCopyV3S ${FULLFEX}
fi

echo  [================--------] 60%%
squashfs_unmake ${SYSTEM} ${ROOTEXTRACTFOLDER}

echo  [====================----] 80%%
extractFWInfo ${ROOTEXTRACTFOLDER} ${SCRIPTFEX} ${INFOTXTTMP}
ln -rs ${BOOTLOGO} ${BOOTLOGO}.jpg 
ln -rs ${SLOGO} ${SLOGO}.jpg 

echo  [========================] 100%%


#echo Auto FWINFO script by nutsey v0.4.
#echo.
#echo  [================--------] 60%%
#cls
#echo Auto FWINFO script by nutsey v0.4.
#echo.
#echo  [====================----] 80%%
#call "fwinfo.cmd"
#cls
#echo Auto FWINFO script by nutsey v0.4.
#echo.
#echo  [========================] 100%%
#echo.
#echo Done!
#echo.
#cls
#echo Auto FWINFO script by nutsey v0.4.
#echo.
#type FWINFO\fwinfo.txt
#echo.
#echo Files FWINFO.TXT, ON-LOGO.JPG and OFF-LOGO.JPG saved to FWINFO folder.
#echo.
#:PROMPT
#echo Do you want to delete extracted temp files?
#set INPUT=
#set /P INPUT=(y/n): %=%
#If /I "%INPUT%"=="y" goto yes 
#If /I "%INPUT%"=="n" exit
#echo Press "y" or "n" and hit Enter & GOTO PROMPT
#:FEXNOTFOUND
#echo FULL_IMG.FEX not found!
#echo.
#echo Put FULL_IMG.FEX file from your firmware backup to the scripts folder. Press any key to exit...
#pause>nul
#exit
#:SFKNOTFOUND
#echo SFK.EXE not found - Please download it from: https://sourceforge.net/projects/swissfileknife/
#echo.
#echo Press any key to exit...
#pause>nul
#exit
#:YES
#del "script.bin" >nul 2>&1
#del "script.fex" >nul 2>&1
#rd /S /Q UNFEX >nul 2>&1
#rd /S /Q squashfs-root >nul 2>&1
#echo Extracted file deleted. Press any key to exit...
#pause>nul
#exit
